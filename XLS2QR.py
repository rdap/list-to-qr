import sys
import os
from xls_to_qr import xls_to_qr, get_sheet_names, get_col_names
from os.path import expanduser
if sys.version_info < (3, 0):
    # Python 2
    from Tkinter import *
    import ScrolledText as scrolledtext
    import tkMessageBox as messagebox
    from ttk import Progressbar, Combobox
    import tkFileDialog as filedialog
else:
    # Python 3
    from tkinter import *
    from tkinter import scrolledtext
    from tkinter import messagebox
    from tkinter.ttk import Progressbar, Combobox
    from tkinter import filedialog


class MainGUI:

    def __init__(self):
        self._window = Tk()
        self._window.title("XLS2QR")
        row = 0
        self._title = Label(self._window, text="Create QR codes for texts in an excel column", font='Helvetica 18 bold')
        self._title.grid(column=0, row=row, columnspan=4, sticky=W + E + N + S, padx=40, pady=20)
        row += 1
        self._var_file, self._file_entry = self._add_browser(self._window, 'Excel file', row, self._file_clicked)
        row += 1
        self._var_sheet, self._sheet_entry = self._add_combobox(self._window, "Sheet name", row)
        row += 1
        self._var_col, self._col_entry = self._add_combobox(self._window, "Column title", row)
        row += 1
        self._var_out, self._out_entry = self._add_browser(self._window, 'Output directory', row, self._out_clicked)
        row += 1
        self._chk_replace_state = BooleanVar()
        self._chk_replace_state.set(True)
        self._chk_replace = Checkbutton(self._window, text='Replace existing QR files', var=self._chk_replace_state)
        self._chk_replace.grid(column=0, row=row, columnspan=4, sticky=W, padx=4, pady=5)
        row += 1
        self._btn = Button(self._window, text="Create QR Codes", command=self._clicked, state='disabled')
        self._btn.grid(column=0, row=row, pady=row, padx=5, columnspan=4, sticky=W + E + N + S)
        row += 1
        self._bar = Progressbar(self._window, length=400)
        self._bar.grid(column=0, row=row, padx=5, pady=5, columnspan=4, sticky=W + E + N + S)
        row += 1
        self._status = scrolledtext.ScrolledText(self._window, width=63, height=10, state="disabled")
        self._status.grid(column=0, row=row, padx=5, pady=5, columnspan=4, sticky=W + N + S)
        self._center(self._window)
        self._window.lift()
        self._window.mainloop()

    def _add_browser(self, window, text, row, command):
        var = StringVar(window)
        var.trace("w", self._trace_filled)
        lbl = Label(window, text=text)
        lbl.grid(column=0, row=row, sticky=W, padx=5, pady=5)
        col = Label(window, text=":")
        col.grid(column=1, row=row, pady=5)
        entry = Entry(window, width=22, textvariable=var)
        entry.grid(column=2, row=row, pady=5, sticky=W)
        button = Button(window, text="Browse...", command=command)
        button.grid(column=3, row=row, padx=5, pady=5, sticky=W)
        return var, entry

    def _add_combobox(self, window, text, row):
        var = StringVar(window)
        var.trace("w", self._trace_filled)
        lbl = Label(self._window, text=text)
        lbl.grid(column=0, row=row, sticky=W, padx=5, pady=5)
        col = Label(self._window, text=":")
        col.grid(column=1, row=row, pady=5)
        entry = Combobox(self._window, width=20, textvariable=var, state='disabled')
        entry.grid(column=2, row=row, pady=5, columnspan=2, sticky=W)
        return var, entry

    def _set_text(self, entry, text):
        entry.delete(0,END)
        entry.insert(0,text)
        entry.xview(END)

    def _clicked(self):
        print('Clicked')
        success = xls_to_qr(self._var_file.get(), self._var_sheet.get(), self._var_col.get(), self._var_out.get(), self,
                            replace=self._chk_replace_state.get())
        if success:
            self.log("Done!")
        else:
            self.log("Processing error, please check the input values.")

    def _file_clicked(self):
        file_path = filedialog.askopenfilename(filetypes = (("Excel files","*.xls"),
                                                          ("Excel files","*.xlsx"),
                                                          ("all files","*.*")))
        self._set_text(self._file_entry, file_path)

    def _out_clicked(self):
        home = expanduser("~")
        out_path = filedialog.askdirectory(initialdir=home)
        self._set_text(self._out_entry, out_path)

    def _trace_filled(self, *args):
        if self._var_file.get() and os.path.isfile(self._var_file.get()):
            sheet_names = get_sheet_names(self._var_file.get())
            self._sheet_entry['values'] = sheet_names
            self._sheet_entry.config(state='readonly')
        else:
            self._sheet_entry.set('')
            self._sheet_entry.config(state='disabled')
        if self._var_sheet.get():
            col_names = get_col_names(self._var_file.get(), self._var_sheet.get())
            self._col_entry['values'] = col_names
            self._col_entry.config(state='readonly')
        else:
            self._col_entry.set('')
            self._col_entry.config(state='disabled')
        if self._var_file.get() and self._var_out.get() and self._var_col.get() and self._var_sheet.get() \
                and os.path.isdir(self._var_out.get()):
            self._btn.config(state='normal')
        else:
            self._btn.config(state='disabled')

    def _center(self, root):
        window_width = root.winfo_reqwidth()
        window_height = root.winfo_reqheight()
        # Gets both half the screen width/height and window width/height
        positionRight = int(root.winfo_screenwidth() / 2 - window_width / 2)
        positionDown = int(root.winfo_screenheight() / 2 - window_height / 2)
        # Positions the window in the center of the page.
        root.geometry("+{}+{}".format(positionRight, positionDown))

    def log(self, msg):
        self._status.configure(state='normal')
        self._status.insert(END, msg + '\n')
        self._status.configure(state='disabled')
        self._status.yview(END)

    def progress(self, value):
        self._bar['value'] = value

    def error(self, exception):
        self.log('Error encountered')
        self.log(str(exception))
        messagebox.showerror('Error', exception)



if __name__ == "__main__":
    MainGUI()
