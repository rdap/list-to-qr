import pandas as pd
from PIL import ImageDraw
from PIL import ImageFont
import sys
import qrcode
import os
import traceback


def get_sheet_names(file_path):
    xl = pd.ExcelFile(file_path)
    return xl.sheet_names


def get_col_names(file_path, sheet_name):
    df = pd.read_excel(file_path, sheet_name=sheet_name)
    return list(df)


def xls_to_qr(file_path, sheet_name, column_name, out_path, logger=None, replace=True):
    if logger:
        logger.log('Reading list of:')
        logger.log(' : '.join(['Excel file', file_path]))
        logger.log(' : '.join(['Sheet', sheet_name]))
        logger.log(' : '.join(['Column', column_name]))
        logger.log('--------------------------------')
    try:
        df = pd.read_excel(file_path, sheet_name=sheet_name)
        font = ImageFont.load_default()
        if logger:
            logger.progress(0)
            logger.log(' '.join(['Creating QR files in',out_path]))
            logger.log('--------------------------------')
        for i in df.index:
            name = df[column_name][i]
            outfile = os.path.join(out_path, name+'.jpg')
            outfile_exist = os.path.isfile(outfile)
            if not outfile_exist or replace:
                img = qrcode.make(name)
                draw = ImageDraw.Draw(img)
                W, H = img.size
                w, h = draw.textsize(name, font=font)
                draw.text(((W-w)/2,H-h-5), name, font=font)
                img.save(outfile)
                if logger:
                    if outfile_exist:
                        logger.log(' : '.join([name, outfile + ' replaced']))
                    else:
                        logger.log(' : '.join([name, outfile]))
                    logger.progress(((i+1)/len(df.index))*100)
            else:
                if logger:
                    logger.log(' : '.join([name, outfile + ' exists, skipped']))
                    logger.progress(((i+1)/len(df.index))*100)
        logger.log('--------------------------------')
        return True
    except Exception:
        traceback.print_exc()
        if logger:
            logger.error(Exception)
        logger.log('--------------------------------')
        return False


if __name__ == "__main__":
    file_path = sys.argv[1]
    sheet_name = sys.argv[2]
    column_name = sys.argv[3]
    out_path = sys.argv[4]
    xls_to_qr(file_path, sheet_name, column_name, out_path)
        
