# XLS2QR

## Description
A utility tool to create QR codes from list of text in an excel file.

## Input/Output
It takes an excel file as input, where the first row of the sheet should contain the column names.

For each text in the cells of a specified column, it will create an image file of their respective QR code.

## Prerequisites
- [qrcode](https://github.com/lincolnloop/python-qrcode)
- [pandas](https://pandas.pydata.org)

## Usage
```
python XLS2QR.py
```

## Distribution
Distribution package for MacOS is [available](XLS2QR.zip), although it has not been tested, thus might not work properly on all MacOS environments.



